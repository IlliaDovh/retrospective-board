export interface Comment {
  id: string;
  text: string;
}

export interface Card {
  id: string;
  text: string;
  votesNumber: Array<number>;
  comments: Comment[];
}

export interface Column {
  id: string;
  name: string;
  cards: Card[];
}

export interface Board {
  id: string;
  name: string;
  columns: Column[];
}
