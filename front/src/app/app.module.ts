import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NgModule } from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';

import { AddBoardButtonComponent } from './board-list/add-board-button/add-board-button.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { boardsReducer } from './store/boards/boards.reducer';
import { BoardComponent } from './board-list/board/board.component';
import { BoardCreationFormComponent } from './board-creation-form/board-creation-form.component';
import { BoardListComponent } from './board-list/board-list.component';
import { BoardRenamingFormComponent } from './board-list/board-renaming-form/board-renaming-form.component';
import { BoardsEffects } from './store/boards/boards.effects';
import { BoardsService } from './store/boards/boards.service';
import { CardCreationFormComponent } from './board-list/card-creation-form/card-creation-form.component';
import { CardMovingDirective } from './board-list/board/card-moving.directive';
import { CardTextEditingFormComponent } from './board-list/card-text-editing-form/card-text-editing-form.component';
import { ColumnCreationFormComponent } from './board-list/column-creation-form/column-creation-form.component';
import { ColumnMovingDirective } from './board-list/board/column-moving.directive';
import { ColumnRenamingFormComponent } from './board-list/column-renaming-form/column-renaming-form.component';
import { CommentTextEditingFormComponent } from './board-list/comment-text-editing-form/comment-text-editing-form.component';

@NgModule({
  declarations: [
    AddBoardButtonComponent,
    AppComponent,
    BoardComponent,
    BoardCreationFormComponent,
    BoardListComponent,
    BoardRenamingFormComponent,
    CardCreationFormComponent,
    CardMovingDirective,
    CardTextEditingFormComponent,
    ColumnCreationFormComponent,
    ColumnMovingDirective,
    ColumnRenamingFormComponent,
    CommentTextEditingFormComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    EffectsModule.forRoot([BoardsEffects]),
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    StoreDevtoolsModule.instrument(),
    StoreModule.forRoot({
      boards: boardsReducer
    } as any)
  ],
  providers: [BoardsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
