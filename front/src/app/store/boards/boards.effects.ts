import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, OnInitEffects } from '@ngrx/effects';
import { EMPTY, Observable } from 'rxjs';
import { exhaustMap, map, switchMap, switchMapTo, withLatestFrom } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  LoadBoards,
  LoadBoardsSuccess,
  UpdateBoardsOnServer,
  AddBoard,
  RenameBoard,
  DeleteBoard,
  AddColumn,
  RenameColumn,
  MoveColumn,
  DeleteColumn,
  AddCard,
  EditCardText,
  MoveCard,
  DeleteCard,
  AddVote,
  DeleteVote,
  AddComment,
  EditCommentText,
  DeleteComment
} from './boards.actions';
import { BoardsActionType } from './boards.model';
import { BoardsService } from './boards.service';
import { Board } from '../../shared/board.model';
import { StoreState } from '../store.model';
import { selectBoards } from './boards.selectors';

@Injectable()
export class BoardsEffects implements OnInitEffects {

  constructor(
    private actions$: Actions,
    private boardsService: BoardsService,
    private store: Store<StoreState>
  ) { }

  @Effect()
  public loadBoards$: Observable<LoadBoardsSuccess> = this.actions$.pipe(
    ofType(BoardsActionType.LoadBoards),
    exhaustMap(() => this.boardsService.getBoards()),
    map((boards: Board[]) => new LoadBoardsSuccess(boards))
  );

  @Effect()
  public runUpdateBoardsOnAddBoard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.AddBoard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [AddBoard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public updateBoards$: Observable<void> = this.actions$.pipe(
    ofType(BoardsActionType.UpdateBoardsOnServer),
    switchMap((action: UpdateBoardsOnServer) => this.boardsService.updateBoards(action.boards)),
    switchMapTo(EMPTY)
  );

  @Effect()
  public runUpdateCarsOnRenameBoard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.RenameBoard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [RenameBoard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnDeleteBoard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.DeleteBoard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [DeleteBoard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnAddColumn$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.AddColumn),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [AddColumn, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnRenameColumn$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.RenameColumn),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [RenameColumn, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnMoveColumn$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.MoveColumn),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [MoveColumn, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnDeleteColumn$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.DeleteColumn),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [DeleteColumn, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnAddCard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.AddCard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [AddCard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnEditCardText$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.EditCardText),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [EditCardText, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnMoveCard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.MoveCard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [MoveCard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnDeleteCard$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.DeleteCard),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [DeleteCard, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnAddVote$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.AddVote),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [AddVote, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnDeleteVote$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.DeleteVote),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [DeleteVote, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnAddComment$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.AddComment),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [AddComment, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnEditCommentText$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.EditCommentText),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [EditCommentText, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  @Effect()
  public runUpdateCarsOnDeleteComment$: Observable<UpdateBoardsOnServer> = this.actions$.pipe(
    ofType(BoardsActionType.DeleteComment),
    withLatestFrom(this.store.select(selectBoards)),
    map(([, boards]: [DeleteComment, Board[]]) => new UpdateBoardsOnServer(boards))
  );

  ngrxOnInitEffects(): LoadBoards {
    return new LoadBoards();
  }

}
