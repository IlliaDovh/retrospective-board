import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Board } from '../../shared/board.model';

@Injectable()
export class BoardsService {

  constructor(private http: HttpClient) { }

  public getBoards(): Observable<Board[]> {
    return this.http.get<Board[]>('/boards');
  }

  public updateBoards(boards: Board[]): Observable<string> {
    return this.http.post('/boards', boards, {responseType: 'text'});
  }

}
