import { BoardsAction, BoardsActionType } from './boards.model';
import { Board, Column, Card, Comment } from '../../shared/board.model';

export class LoadBoards implements BoardsAction {

  public readonly type: BoardsActionType.LoadBoards = BoardsActionType.LoadBoards;

}

export class LoadBoardsSuccess implements BoardsAction {

  public readonly type: BoardsActionType.LoadBoardsSuccess = BoardsActionType.LoadBoardsSuccess;

  constructor(public readonly boards: Board[]) { }

}

export class AddBoard implements BoardsAction {

  public readonly type: BoardsActionType.AddBoard = BoardsActionType.AddBoard;

  constructor(public readonly board: Board) { }

}

export class UpdateBoardsOnServer implements BoardsAction {

  public readonly type: BoardsActionType.UpdateBoardsOnServer = BoardsActionType.UpdateBoardsOnServer;

  constructor(public readonly boards: Board[]) { }

}

export class RenameBoard implements BoardsAction {

  public readonly type: BoardsActionType.RenameBoard = BoardsActionType.RenameBoard;

  constructor(public readonly boardId: string, public readonly boardNewName: string) { }

}

export class DeleteBoard implements BoardsAction {

  public readonly type: BoardsActionType.DeleteBoard = BoardsActionType.DeleteBoard;

  constructor(public readonly boardId: string) { }

}

export class AddColumn implements BoardsAction {

  public readonly type: BoardsActionType.AddColumn = BoardsActionType.AddColumn;

  constructor(public readonly boardId: string, public readonly column: Column) { }

}

export class RenameColumn implements BoardsAction {

  public readonly type: BoardsActionType.RenameColumn = BoardsActionType.RenameColumn;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly columnNewName: string
  ) { }

}

export class MoveColumn implements BoardsAction {

  public readonly type: BoardsActionType.MoveColumn = BoardsActionType.MoveColumn;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly indexMoveColumnTo: number
  ) { }

}

export class DeleteColumn implements BoardsAction {

  public readonly type: BoardsActionType.DeleteColumn = BoardsActionType.DeleteColumn;

  constructor(public readonly boardId: string, public readonly columnId: string) { }

}

export class AddCard implements BoardsAction {

  public readonly type: BoardsActionType.AddCard = BoardsActionType.AddCard;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly card: Card
  ) { }

}

export class EditCardText implements BoardsAction {

  public readonly type: BoardsActionType.EditCardText = BoardsActionType.EditCardText;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string,
    public readonly cardEditedText: string
  ) { }

}

export class MoveCard implements BoardsAction {

  public readonly type: BoardsActionType.MoveCard = BoardsActionType.MoveCard;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string,
    public readonly columnIndexMoveCardTo: number
  ) { }

}

export class DeleteCard implements BoardsAction {

  public readonly type: BoardsActionType.DeleteCard = BoardsActionType.DeleteCard;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string
  ) { }

}

export class AddVote implements BoardsAction {

  public readonly type: BoardsActionType.AddVote = BoardsActionType.AddVote;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string
  ) { }

}

export class DeleteVote implements BoardsAction {

  public readonly type: BoardsActionType.DeleteVote = BoardsActionType.DeleteVote;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string
  ) { }

}

export class AddComment implements BoardsAction {

  public readonly type: BoardsActionType.AddComment = BoardsActionType.AddComment;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string,
    public readonly comment: Comment
  ) { }

}

export class EditCommentText implements BoardsAction {

  public readonly type: BoardsActionType.EditCommentText = BoardsActionType.EditCommentText;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string,
    public readonly commentId: string,
    public readonly commentEditedText: string
  ) { }

}

export class DeleteComment implements BoardsAction {

  public readonly type: BoardsActionType.DeleteComment = BoardsActionType.DeleteComment;

  constructor(
    public readonly boardId: string,
    public readonly columnId: string,
    public readonly cardId: string,
    public readonly commentId: string
  ) { }

}
