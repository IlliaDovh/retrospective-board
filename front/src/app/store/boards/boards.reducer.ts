import { Board } from '../../shared/board.model';
import { BoardsAction, BoardsActionType } from './boards.model';

export function boardsReducer(state: Board[] = [], action: BoardsAction): Board[] {
  switch (action.type) {
    case BoardsActionType.LoadBoardsSuccess:
      return action.boards!;

    case BoardsActionType.AddBoard:
      return [...state, action.board!];

    case BoardsActionType.RenameBoard: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const renamedBoard = {...board};

          renamedBoard.name = action.boardNewName;
          return renamedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.DeleteBoard:
      return state.filter(board => board.id !== action.boardId);

    case BoardsActionType.AddColumn: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = [...board.columns, action.column];
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.RenameColumn: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const renamedColumn = {...column};

              renamedColumn.name = action.columnNewName;
              return renamedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.MoveColumn: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const boardColumns = board.columns;
          const updatedBoard = {...board};

          boardColumns.forEach((column, index) => {
            const columnId = action.columnId;
            const indexMoveColumnTo = action.indexMoveColumnTo;

            if (column.id === columnId && index !== indexMoveColumnTo) {
              updatedBoard.columns = [...boardColumns];
              updatedBoard.columns = boardColumns.filter(col => col.id !== columnId);
              updatedBoard.columns.splice(indexMoveColumnTo, 0, boardColumns[index]);
            }
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.DeleteColumn: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.filter(column => column.id !== action.columnId);
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.AddCard: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = [...column.cards, action.card];
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.EditCardText: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const editedCard = {...card};

                  editedCard.text = action.cardEditedText;
                  return editedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.MoveCard: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const boardColumns = board.columns;
          const updatedBoard = {...board};

          boardColumns.forEach((column, index) => {
            const columnIndexMoveCardTo = action.columnIndexMoveCardTo;

            if (column.id === action.columnId && index !== columnIndexMoveCardTo) {
              const columnCards = column.cards;
              const cardId = action.cardId;
              const columnMoveCardTo = boardColumns[columnIndexMoveCardTo];
              const cardBeingMoved = columnCards.find(card => card.id === cardId);

              updatedBoard.columns = [...boardColumns];
              updatedBoard.columns[index] = {...column};
              updatedBoard.columns[index].cards = columnCards.filter(card => card.id !== cardId);
              updatedBoard.columns[columnIndexMoveCardTo] = {...columnMoveCardTo};
              updatedBoard.columns[columnIndexMoveCardTo].cards = [...columnMoveCardTo.cards];
              updatedBoard.columns[columnIndexMoveCardTo].cards.push(cardBeingMoved);
            }
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.DeleteCard: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.filter(card => card.id !== action.cardId);
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.AddVote: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const updatedCard = {...card};
                  const cardVotesNumber = card.votesNumber;

                  updatedCard.votesNumber = [...cardVotesNumber];
                  updatedCard.votesNumber.push(cardVotesNumber.length);
                  return updatedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.DeleteVote: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const updatedCard = {...card};

                  updatedCard.votesNumber = [...card.votesNumber];
                  updatedCard.votesNumber.pop();
                  return updatedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.AddComment: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const updatedCard = {...card};

                  updatedCard.comments = [...card.comments, action.comment];
                  return updatedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.EditCommentText: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const updatedCard = {...card};

                  updatedCard.comments = card.comments.map(comment => {
                    if (comment.id === action.commentId) {
                      const updatedComment = {...comment};

                      updatedComment.text = action.commentEditedText;
                      return updatedComment;
                    }
                    return comment;
                  });
                  return updatedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    case BoardsActionType.DeleteComment: {
      return state.map(board => {
        if (board.id === action.boardId) {
          const updatedBoard = {...board};

          updatedBoard.columns = board.columns.map(column => {
            if (column.id === action.columnId) {
              const updatedColumn = {...column};

              updatedColumn.cards = column.cards.map(card => {
                if (card.id === action.cardId) {
                  const updatedCard = {...card};

                  updatedCard.comments = card.comments.filter(comment => comment.id !== action.commentId);
                  return updatedCard;
                }
                return card;
              });
              return updatedColumn;
            }
            return column;
          });
          return updatedBoard;
        }
        return board;
      });
    }

    default:
      return state;
  }
}
