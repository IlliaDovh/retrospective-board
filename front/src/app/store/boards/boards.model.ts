import { Action } from '@ngrx/store';
import { Board, Column, Card, Comment } from '../../shared/board.model';

export enum BoardsActionType {
  LoadBoards = '[Boards] Load Boards',
  LoadBoardsSuccess = '[Boards] Load Boards Success',
  AddBoard = '[Boards] Add Board',
  UpdateBoardsOnServer = '[Boards] Update Boards On Server',
  RenameBoard = '[Boards] Rename Board',
  DeleteBoard = '[Boards] Delete Board',
  AddColumn = '[Boards] Add Column',
  RenameColumn = '[Boards] Rename Column',
  MoveColumn = '[Boards] Move Column',
  DeleteColumn = '[Boards] Delete Column',
  AddCard = '[Boards] Add Card',
  EditCardText = '[Boards] Edit Card Text',
  MoveCard = '[Boards] Move Card',
  DeleteCard = '[Boards] Delete Card',
  AddVote = '[Boards] Add Vote',
  DeleteVote = '[Boards] Delete Vote',
  AddComment = '[Boards] Add Comment',
  EditCommentText = '[Boards] Edit Comment Text',
  DeleteComment = '[Boards] Delete Comment'
}

export interface BoardsAction extends Action {
  type: BoardsActionType;

  boards?: Board[];
  board?: Board;
  boardId?: string;
  boardNewName?: string;
  column?: Column;
  columnId?: string;
  columnNewName?: string;
  indexMoveColumnTo?: number;
  card?: Card;
  cardId?: string;
  cardEditedText?: string;
  columnIndexMoveCardTo?: number;
  comment?: Comment;
  commentId?: string;
  commentEditedText?: string;
}
