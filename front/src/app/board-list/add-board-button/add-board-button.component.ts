import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-board-button',
  templateUrl: './add-board-button.component.html',
  styleUrls: ['./add-board-button.component.scss']
})
export class AddBoardButtonComponent {

  @Output() public boardCreationFormComponentData: EventEmitter<{
    isBoardCreationFormComponentShown: boolean,
    isBoardCreationFormComponentCancelButtonShown?: boolean
  }> = new EventEmitter();

}
