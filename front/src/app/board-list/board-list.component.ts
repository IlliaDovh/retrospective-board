import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Board } from '../shared/board.model';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: [ './board-list.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardListComponent {

  @Input() public boards$!: Observable<Board[]>;

  @Input() public isBoardCreationFormComponentShown!: boolean;

  @Input() public isBoardCreationFormComponentCancelButtonShown!: boolean;

  @Output() public boardCreationFormComponentData: EventEmitter<{
    isBoardCreationFormComponentShown: boolean,
    isBoardCreationFormComponentCancelButtonShown?: boolean
  }> = new EventEmitter();

  isBoardRenamingFormComponentShown: boolean;

  boardId: string;

  boardName: string;

  isColumnCreationFormComponentShown: boolean;

  isColumnRenamingFormComponentShown: boolean;

  columnId: string;

  columnName: string;

  isCardCreationFormComponentShown: boolean;

  isCardTextEditingFormComponentShown: boolean;

  cardId: string;

  cardText: string;

  commentCreationSectionCardId: string;

  isCommentCreationSectionShown: boolean;

  cardIds: object = {};

  isCommentTextEditingFormComponentShown: boolean;

  commentId: string;

  commentText: string;

  public getBoardCreationFormComponentData({
    isBoardCreationFormComponentShown,
    isBoardCreationFormComponentCancelButtonShown
  }: {
    isBoardCreationFormComponentShown: boolean,
    isBoardCreationFormComponentCancelButtonShown?: boolean
  }): void {
    this.boardCreationFormComponentData.emit({
      isBoardCreationFormComponentShown,
      isBoardCreationFormComponentCancelButtonShown
    });
    this.isBoardRenamingFormComponentShown = false;
    this.isColumnCreationFormComponentShown = false;
    this.isColumnRenamingFormComponentShown = false;
    this.isCardCreationFormComponentShown = false;
    this.isCardTextEditingFormComponentShown = false;
    this.isCommentTextEditingFormComponentShown = false;
  }

  public hideBoardOpenFormComponent(boardId: string): void {
    if (this.boardId === boardId) {
      this.isBoardRenamingFormComponentShown = false;
      this.isColumnCreationFormComponentShown = false;
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

  public getBoardRenamingFormComponentData({
    isBoardRenamingFormComponentShown,
    boardId,
    boardName
  }: {
    isBoardRenamingFormComponentShown: boolean,
    boardId?: string,
    boardName?: string
  }): void {
    this.isBoardRenamingFormComponentShown = isBoardRenamingFormComponentShown;
    if (isBoardRenamingFormComponentShown) {
      this.boardId = boardId;
      this.boardName = boardName;
      this.isColumnCreationFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
  }

  public hideBoardRenamingFormComponent(): void {
    this.isBoardRenamingFormComponentShown = false;
  }

  public getColumnCreationFormComponentData({
    isColumnCreationFormComponentShown,
    boardId
  }: {
    isColumnCreationFormComponentShown: boolean,
    boardId?: string
  }): void {
    this.isColumnCreationFormComponentShown = isColumnCreationFormComponentShown;
    if (isColumnCreationFormComponentShown) {
      this.boardId = boardId;
      this.isBoardRenamingFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
  }

  public hideColumnCreationFormComponent(): void {
    this.isColumnCreationFormComponentShown = false;
  }

  public hideColumnOpenFormComponent(columnId: string): void {
    if (this.columnId === columnId) {
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

  public getColumnRenamingFormComponentData({
    isColumnRenamingFormComponentShown,
    boardId,
    columnId,
    columnName
  }: {
    isColumnRenamingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    columnName?: string
  }): void {
    this.isColumnRenamingFormComponentShown = isColumnRenamingFormComponentShown;
    if (isColumnRenamingFormComponentShown) {
      this.boardId = boardId;
      this.columnId = columnId;
      this.columnName = columnName;
      this.isBoardRenamingFormComponentShown = false;
      this.isColumnCreationFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
  }

  public hideColumnRenamingFormComponent(): void {
    this.isColumnRenamingFormComponentShown = false;
  }

  public getCardCreationFormComponentData({
    isCardCreationFormComponentShown,
    boardId,
    columnId
  }: {
    isCardCreationFormComponentShown: boolean,
    boardId?: string,
    columnId?: string
  }): void {
    this.isCardCreationFormComponentShown = isCardCreationFormComponentShown;
    if (isCardCreationFormComponentShown) {
      this.boardId = boardId;
      this.columnId = columnId;
      this.isBoardRenamingFormComponentShown = false;
      this.isColumnCreationFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isColumnRenamingFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
  }

  public hideCardCreationFormComponent(): void {
    this.isCardCreationFormComponentShown = false;
  }

  public hideCardOpenFormComponent(cardId: string): void {
    if (this.cardId === cardId) {
      this.isCardTextEditingFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

  public getCardTextEditingFormComponentData({
    isCardTextEditingFormComponentShown,
    boardId,
    columnId,
    cardId,
    cardText
  }: {
    isCardTextEditingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    cardId?: string,
    cardText?: string
  }): void {
    this.isCardTextEditingFormComponentShown = isCardTextEditingFormComponentShown;
    if (isCardTextEditingFormComponentShown) {
      this.boardId = boardId;
      this.columnId = columnId;
      this.cardId = cardId;
      this.cardText = cardText;
      this.isBoardRenamingFormComponentShown = false;
      this.isColumnCreationFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCommentTextEditingFormComponentShown = false;
    }
  }

  public hideCardTextEditingFormComponent(): void {
    this.isCardTextEditingFormComponentShown = false;
  }

  public getCommentCreationSectionData({
    cardId,
    isCommentCreationSectionShown,
    commentFieldValue
  }: {
    cardId?: string,
    isCommentCreationSectionShown?: boolean,
    commentFieldValue?: string
  }): void {
    if (cardId) {
      this.commentCreationSectionCardId = cardId;
    }
    if (typeof isCommentCreationSectionShown === 'boolean') {
      this.isCommentCreationSectionShown = isCommentCreationSectionShown;
    }
    if (cardId && !this.cardIds[cardId]) {
      this.cardIds[cardId] = null;
    }
    if (typeof commentFieldValue === 'string') {
      this.cardIds[cardId] = commentFieldValue;
    }
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

  public hideCommentOpenFormComponent(commentId: string): void {
    if (this.commentId === commentId) {
      this.isCommentTextEditingFormComponentShown = false;
    }
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

  public getCommentTextEditingFormComponentData({
    isCommentTextEditingFormComponentShown,
    boardId,
    columnId,
    cardId,
    commentId,
    commentText
  }: {
    isCommentTextEditingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    cardId?: string,
    commentId?: string,
    commentText?: string
  }): void {
    this.isCommentTextEditingFormComponentShown = isCommentTextEditingFormComponentShown;
    if (isCommentTextEditingFormComponentShown) {
      this.boardId = boardId;
      this.columnId = columnId;
      this.cardId = cardId;
      this.commentId = commentId;
      this.commentText = commentText;
      this.isBoardRenamingFormComponentShown = false;
      this.isColumnCreationFormComponentShown = false;
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: false});
      this.isColumnRenamingFormComponentShown = false;
      this.isCardCreationFormComponentShown = false;
      this.isCardTextEditingFormComponentShown = false;
    }
  }

  public hideCommentTextEditingFormComponent(): void {
    this.isCommentTextEditingFormComponentShown = false;
  }

  public checkIfBoardCreationFormComponentShown(): void {
    if (this.isBoardCreationFormComponentShown) {
      this.boardCreationFormComponentData.emit({isBoardCreationFormComponentShown: true});
    }
  }

}
