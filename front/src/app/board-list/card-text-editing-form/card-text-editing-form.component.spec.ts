import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardTextEditingFormComponent } from './card-text-editing-form.component';

describe('CardTextEditingFormComponent', () => {
  let component: CardTextEditingFormComponent;
  let fixture: ComponentFixture<CardTextEditingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardTextEditingFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardTextEditingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
