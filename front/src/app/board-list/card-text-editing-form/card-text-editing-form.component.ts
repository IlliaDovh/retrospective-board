import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { StoreState } from 'src/app/store/store.model';
import { EditCardText } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-card-text-editing-form',
  templateUrl: './card-text-editing-form.component.html',
  styleUrls: ['./card-text-editing-form.component.scss']
})
export class CardTextEditingFormComponent implements OnInit {

  @Input() public cardText!: string;

  @Input() public boardId!: string;

  @Input() public columnId!: string;

  @Input() public cardId!: string;

  @Output() public hideCardTextEditingFormComponent: EventEmitter<any> = new EventEmitter();

  public readonly cardTextEditingForm: FormGroup = new FormGroup({
    text: new FormControl('')
  });

  constructor(private store: Store<StoreState>) { }

  ngOnInit(): void {
    this.cardTextEditingForm.setValue({text: this.cardText});
  }

  public editCardText(): void {
    this.store.dispatch(new EditCardText(
      this.boardId,
      this.columnId,
      this.cardId,
      this.cardTextEditingForm.value.text
    ));
    this.hideCardTextEditingFormComponent.emit();
  }

}
