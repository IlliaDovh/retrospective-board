import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { Store } from '@ngrx/store';
import { StoreState } from '../../store/store.model';
import { MoveCard } from '../../store/boards/boards.actions';

@Directive({
  selector: '[appCardMoving]'
})
export class CardMovingDirective {

  @Input() boardColumnsAmount!: number;

  @Input() boardId!: string;

  @Input() columnId!: string;

  @Input() cardId!: string;

  constructor(
    private renderer: Renderer2,
    private card: ElementRef,
    private store: Store<StoreState>
  ) { }

  isCardMoving: boolean;

  onMousedownX: number;

  onMousedownY: number;

  @HostListener('mousedown', ['$event.type', '$event.x', '$event.y', '$event.button', '$event.stopPropagation()'])

  @HostListener('mousemove', ['$event.type', '$event.x', '$event.y'])

  @HostListener('mouseup', ['$event.type', '$event.x'])

  onMouseEvent(eventType: string, x: number, y: number, mouseButtonCode: number): void {
    switch (eventType) {
      case 'mousedown': {
        const mouseLeftButtonCode = 0;

        if (mouseButtonCode === mouseLeftButtonCode) {
          this.renderer.addClass(this.card.nativeElement, 'on-mousedown');
          this.isCardMoving = true;
          this.onMousedownX = x;
          this.onMousedownY = y;
        }
        break;
      }

      case 'mousemove': {
        if (this.isCardMoving) {
          this.renderer.setStyle(this.card.nativeElement, 'left', `${x - this.onMousedownX}px`);
          this.renderer.setStyle(this.card.nativeElement, 'top', `${y - this.onMousedownY}px`);
        }
        break;
      }

      case 'mouseup': {
        if (this.isCardMoving) {
          this.renderer.removeClass(this.card.nativeElement, 'on-mousedown');
          const bodySideMargins = 16;
          const screenWidth = document.documentElement.clientWidth;
          let clientX = x - bodySideMargins / 2;
          const columnArea = (screenWidth - bodySideMargins) / this.boardColumnsAmount;
          let columnIndexMoveCardTo = 0;

          while (clientX - columnArea > 0) {
            columnIndexMoveCardTo++;
            if (columnIndexMoveCardTo + 1 > this.boardColumnsAmount) {
              columnIndexMoveCardTo--;
              break;
            }
            clientX -= columnArea;
          }
          this.renderer.removeStyle(this.card.nativeElement, 'left');
          this.renderer.removeStyle(this.card.nativeElement, 'top');
          this.store.dispatch(new MoveCard(
            this.boardId,
            this.columnId,
            this.cardId,
            columnIndexMoveCardTo
          ));
          this.isCardMoving = false;
        }
        break;
      }

      default:
        break;
    }
  }

}
