import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { Store } from '@ngrx/store';
import { StoreState } from '../../store/store.model';
import { MoveColumn } from '../../store/boards/boards.actions';

@Directive({
  selector: '[appColumnMoving]'
})
export class ColumnMovingDirective {

  @Input() boardColumnsAmount!: number;

  @Input() boardId!: string;

  @Input() columnId!: string;

  constructor(
    private renderer: Renderer2,
    private column: ElementRef,
    private store: Store<StoreState>
  ) { }

  isColumnMoving: boolean;

  onMousedownX: number;

  onMousedownY: number;

  @HostListener('mousedown', ['$event.type', '$event.x', '$event.y', '$event.button'])

  @HostListener('mousemove', ['$event.type', '$event.x', '$event.y'])

  @HostListener('mouseup', ['$event.type', '$event.x'])

  onMouseEvent(eventType: string, x: number, y: number, mouseButtonCode: number): void {
    switch (eventType) {
      case 'mousedown': {
        const mouseLeftButtonCode = 0;

        if (mouseButtonCode === mouseLeftButtonCode) {
          this.renderer.addClass(this.column.nativeElement, 'on-mousedown');
          this.isColumnMoving = true;
          this.onMousedownX = x;
          this.onMousedownY = y;
        }
        break;
      }

      case 'mousemove': {
        if (this.isColumnMoving) {
          this.renderer.setStyle(this.column.nativeElement, 'left', `${x - this.onMousedownX}px`);
          this.renderer.setStyle(this.column.nativeElement, 'top', `${y - this.onMousedownY}px`);
        }
        break;
      }

      case 'mouseup': {
        if (this.isColumnMoving) {
          this.renderer.removeClass(this.column.nativeElement, 'on-mousedown');
          const bodySideMargins = 16;
          const screenWidth = document.documentElement.clientWidth;
          let clientX = x - bodySideMargins / 2;
          const columnArea = (screenWidth - bodySideMargins) / this.boardColumnsAmount;
          let indexMoveColumnTo = 0;

          while (clientX - columnArea > 0) {
            indexMoveColumnTo++;
            if (indexMoveColumnTo + 1 > this.boardColumnsAmount) {
              indexMoveColumnTo--;
              break;
            }
            clientX -= columnArea;
          }
          this.renderer.removeStyle(this.column.nativeElement, 'left');
          this.renderer.removeStyle(this.column.nativeElement, 'top');
          this.store.dispatch(new MoveColumn(
            this.boardId,
            this.columnId,
            indexMoveColumnTo
          ));
          this.isColumnMoving = false;
        }
        break;
      }

      default:
        break;
    }
  }

}
