import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { v4 as uuid } from 'uuid';
import { Board } from '../../shared/board.model';
import { StoreState } from '../../store/store.model';
import { DeleteBoard, DeleteColumn, DeleteCard, AddVote, DeleteVote, AddComment, DeleteComment } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: [ './board.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardComponent implements OnInit {

  @Input() public board!: Board;

  @Input() public cardId!: string;

  @Input() public cardIds!: object;

  @Input() public boards$!: Observable<Board[]>;

  @Input() public isCommentCreationSectionShown!: boolean;

  @Output() public boardCreationFormComponentData: EventEmitter<{
    isBoardCreationFormComponentShown: boolean,
    isBoardCreationFormComponentCancelButtonShown?: boolean
  }> = new EventEmitter();

  @Output() public hideBoardOpenFormComponent: EventEmitter<string> = new EventEmitter();

  @Output() public boardRenamingFormComponentData: EventEmitter<{
    isBoardRenamingFormComponentShown: boolean,
    boardId?: string,
    boardName?: string
  }> = new EventEmitter();

  @Output() public columnCreationFormComponentData: EventEmitter<{
    isColumnCreationFormComponentShown: boolean,
    boardId?: string
  }> = new EventEmitter();

  @Output() public hideColumnOpenFormComponent: EventEmitter<string> = new EventEmitter();

  @Output() public columnRenamingFormComponentData: EventEmitter<{
    isColumnRenamingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    columnName?: string
  }> = new EventEmitter();

  @Output() public cardCreationFormComponentData: EventEmitter<{
    isCardCreationFormComponentShown: boolean,
    boardId?: string,
    columnId?: string
  }> = new EventEmitter();

  @Output() public hideCardOpenFormComponent: EventEmitter<string> = new EventEmitter();

  @Output() public cardTextEditingFormComponentData: EventEmitter<{
    isCardTextEditingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    cardId?: string,
    cardText?: string
  }> = new EventEmitter();

  @Output() public commentCreationSectionData: EventEmitter<{
    cardId?: string,
    isCommentCreationSectionShown?: boolean,
    commentFieldValue?: string
  }> = new EventEmitter();

  @Output() public hideCommentOpenFormComponent: EventEmitter<string> = new EventEmitter();

  @Output() public commentTextEditingFormComponentData: EventEmitter<{
    isCommentTextEditingFormComponentShown: boolean,
    boardId?: string,
    columnId?: string,
    cardId?: string,
    commentId?: string,
    commentText?: string
  }> = new EventEmitter();

  @Output() public checkIfBoardCreationFormComponentShown: EventEmitter<any> = new EventEmitter();

  public readonly commentCreationForm: FormGroup = new FormGroup({
    text: new FormControl('')
  });

  constructor(private store: Store<StoreState>) { }

  ngOnInit(): void {
    if (this.cardId) {
      this.commentCreationForm.setValue({text: this.cardIds[this.cardId]});
    }
    this.commentCreationForm.get('text').valueChanges.subscribe(commentFieldValue => {
      this.commentCreationSectionData.emit({cardId: this.cardId, commentFieldValue});
    });
  }

  public sendBoardRenamingFormComponentData(boardId: string, boardName: string): void {
    this.boardRenamingFormComponentData.emit({isBoardRenamingFormComponentShown: false});
    setTimeout(() => this.boardRenamingFormComponentData.emit({
      isBoardRenamingFormComponentShown: true,
      boardId,
      boardName
    }));
  }

  public deleteBoard(boardId: string): void {
    this.store.dispatch(new DeleteBoard(boardId));
    this.boards$.subscribe(boards => {
      if (!boards.length) {
        this.boardCreationFormComponentData.emit({
          isBoardCreationFormComponentShown: true,
          isBoardCreationFormComponentCancelButtonShown: false
        });
      } else {
        this.hideBoardOpenFormComponent.emit(boardId);
      }
    });
  }

  public sendColumnCreationFormComponentData(boardId: string): void {
    this.columnCreationFormComponentData.emit({isColumnCreationFormComponentShown: false});
    setTimeout(() => this.columnCreationFormComponentData.emit({
      isColumnCreationFormComponentShown: true,
      boardId
    }));
  }

  public sendColumnRenamingFormComponentData(boardId: string, columnId: string, columnName: string): void {
    this.columnRenamingFormComponentData.emit({isColumnRenamingFormComponentShown: false});
    setTimeout(() => this.columnRenamingFormComponentData.emit({
      isColumnRenamingFormComponentShown: true,
      boardId,
      columnId,
      columnName
    }));
  }

  public deleteColumn(boardId: string, columnId: string): void {
    this.store.dispatch(new DeleteColumn(boardId, columnId));
    this.hideColumnOpenFormComponent.emit(columnId);
  }

  public sendCardCreationFormComponentData(boardId: string, columnId: string): void {
    this.cardCreationFormComponentData.emit({isCardCreationFormComponentShown: false});
    setTimeout(() => this.cardCreationFormComponentData.emit({
      isCardCreationFormComponentShown: true,
      boardId,
      columnId
    }));
  }

  public sendCardTextEditingFormComponentData(boardId: string, columnId: string, cardId: string, cardText: string): void {
    this.cardTextEditingFormComponentData.emit({isCardTextEditingFormComponentShown: false});
    setTimeout(() => this.cardTextEditingFormComponentData.emit({
      isCardTextEditingFormComponentShown: true,
      boardId,
      columnId,
      cardId,
      cardText
    }));
  }

  public deleteCard(boardId: string, columnId: string, cardId: string): void {
    this.store.dispatch(new DeleteCard(boardId, columnId, cardId));
    this.hideCardOpenFormComponent.emit(cardId);
  }

  public addVote(boardId: string, columnId: string, cardId: string): void {
    this.store.dispatch(new AddVote(boardId, columnId, cardId));
  }

  public deleteVote(boardId: string, columnId: string, cardId: string): void {
    this.store.dispatch(new DeleteVote(boardId, columnId, cardId));
  }

  public writeComment(cardId: string): void {
    this.cardId = cardId;
    this.commentCreationSectionData.emit({cardId, isCommentCreationSectionShown: true});
    this.commentCreationForm.setValue({text: this.cardIds[cardId]});
  }

  public addComment(boardId: string, columnId: string, cardId: string): void {
    this.store.dispatch(new AddComment(
      boardId,
      columnId,
      cardId,
      {
        id: uuid(),
        ...this.commentCreationForm.value
      }
    ));
    this.commentCreationSectionData.emit({cardId, isCommentCreationSectionShown: true});
    this.commentCreationForm.setValue({text: ''});
  }

  public sendCommentTextEditingFormComponentData(
    boardId: string,
    columnId: string,
    cardId: string,
    commentId: string,
    commentText: string
  ): void {
    this.commentTextEditingFormComponentData.emit({isCommentTextEditingFormComponentShown: false});
    setTimeout(() => this.commentTextEditingFormComponentData.emit({
      isCommentTextEditingFormComponentShown: true,
      boardId,
      columnId,
      cardId,
      commentId,
      commentText
    }));
  }

  public deleteComment(boardId: string, columnId: string, cardId: string, commentId: string): void {
    this.store.dispatch(new DeleteComment(boardId, columnId, cardId, commentId));
    this.hideCommentOpenFormComponent.emit(commentId);
  }

}
