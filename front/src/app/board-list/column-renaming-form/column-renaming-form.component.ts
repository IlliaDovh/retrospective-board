import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { StoreState } from 'src/app/store/store.model';
import { RenameColumn } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-column-renaming-form',
  templateUrl: './column-renaming-form.component.html',
  styleUrls: ['./column-renaming-form.component.scss']
})
export class ColumnRenamingFormComponent implements OnInit {

  @Input() public columnName!: string;

  @Input() public boardId!: string;

  @Input() public columnId!: string;

  @Output() public hideColumnRenamingFormComponent: EventEmitter<any> = new EventEmitter();

  public readonly columnRenamingForm: FormGroup = new FormGroup({
    name: new FormControl('')
  });

  constructor(private store: Store<StoreState>) { }

  ngOnInit(): void {
    this.columnRenamingForm.setValue({name: this.columnName});
  }

  public renameColumn(): void {
    this.store.dispatch(new RenameColumn(
      this.boardId,
      this.columnId,
      this.columnRenamingForm.value.name
    ));
    this.hideColumnRenamingFormComponent.emit();
  }

}
