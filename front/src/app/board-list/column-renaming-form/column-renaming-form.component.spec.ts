import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnRenamingFormComponent } from './column-renaming-form.component';

describe('ColumnNameEditingModalComponent', () => {
  let component: ColumnRenamingFormComponent;
  let fixture: ComponentFixture<ColumnRenamingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumnRenamingFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnRenamingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
