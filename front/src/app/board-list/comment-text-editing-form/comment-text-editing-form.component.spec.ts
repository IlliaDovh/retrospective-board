import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentTextEditingFormComponent } from './comment-text-editing-form.component';

describe('CommentTextEditingFormComponent', () => {
  let component: CommentTextEditingFormComponent;
  let fixture: ComponentFixture<CommentTextEditingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentTextEditingFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentTextEditingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
