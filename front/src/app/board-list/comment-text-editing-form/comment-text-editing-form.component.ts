import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { StoreState } from 'src/app/store/store.model';
import { EditCommentText } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-comment-text-editing-form',
  templateUrl: './comment-text-editing-form.component.html',
  styleUrls: ['./comment-text-editing-form.component.scss']
})
export class CommentTextEditingFormComponent implements OnInit {

  @Input() public commentText!: string;

  @Input() public boardId!: string;

  @Input() public columnId!: string;

  @Input() public cardId!: string;

  @Input() public commentId!: string;

  @Output() public hideCommentTextEditingFormComponent: EventEmitter<any> = new EventEmitter();

  public readonly commentTextEditingForm: FormGroup = new FormGroup({
    text: new FormControl('')
  });

  constructor(private store: Store<StoreState>) { }

  ngOnInit(): void {
    this.commentTextEditingForm.setValue({text: this.commentText});
  }

  public editCommentText(): void {
    this.store.dispatch(new EditCommentText(
      this.boardId,
      this.columnId,
      this.cardId,
      this.commentId,
      this.commentTextEditingForm.value.text
    ));
    this.hideCommentTextEditingFormComponent.emit();
  }

}
