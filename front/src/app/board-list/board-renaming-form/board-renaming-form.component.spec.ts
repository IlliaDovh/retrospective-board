import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardRenamingFormComponent } from './board-renaming-form.component';

describe('BoardRenamingFormComponent', () => {
  let component: BoardRenamingFormComponent;
  let fixture: ComponentFixture<BoardRenamingFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardRenamingFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardRenamingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
