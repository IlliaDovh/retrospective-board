import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { StoreState } from 'src/app/store/store.model';
import { RenameBoard } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-board-renaming-form',
  templateUrl: './board-renaming-form.component.html',
  styleUrls: ['./board-renaming-form.component.scss']
})
export class BoardRenamingFormComponent implements OnInit {

  @Input() public boardName!: string;

  @Input() public boardId!: string;

  @Output() public hideBoardRenamingFormComponent: EventEmitter<any> = new EventEmitter();

  public readonly boardRenamingForm: FormGroup = new FormGroup({
    name: new FormControl('')
  });

  constructor(private store: Store<StoreState>) { }

  ngOnInit(): void {
    this.boardRenamingForm.setValue({name: this.boardName});
  }

  public renameBoard(): void {
    this.store.dispatch(new RenameBoard(
      this.boardId,
      this.boardRenamingForm.value.name
    ));
    this.hideBoardRenamingFormComponent.emit();
  }

}
