import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { v4 as uuid } from 'uuid';
import { FormControl, FormGroup } from '@angular/forms';
import { StoreState } from 'src/app/store/store.model';
import { AddColumn } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-column-creation-form',
  templateUrl: './column-creation-form.component.html',
  styleUrls: ['./column-creation-form.component.scss']
})
export class ColumnCreationFormComponent {

  @Input() public boardId!: string;

  @Output() public hideColumnCreationFormComponent: EventEmitter<any> = new EventEmitter();

  constructor(private store: Store<StoreState>) { }

  public readonly columnCreationForm: FormGroup = new FormGroup({
    name: new FormControl('')
  });

  public addColumn(): void {
    this.store.dispatch(new AddColumn(
      this.boardId,
      {
        id: uuid(),
        ...this.columnCreationForm.value,
        cards: []
      }
    ));
    this.hideColumnCreationFormComponent.emit();
  }

}
