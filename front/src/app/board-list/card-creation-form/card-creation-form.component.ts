import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { v4 as uuid } from 'uuid';
import { FormControl, FormGroup } from '@angular/forms';
import { StoreState } from 'src/app/store/store.model';
import { AddCard } from '../../store/boards/boards.actions';

@Component({
  selector: 'app-card-creation-form',
  templateUrl: './card-creation-form.component.html',
  styleUrls: ['./card-creation-form.component.scss']
})
export class CardCreationFormComponent {

  @Input() public boardId!: string;

  @Input() public columnId!: string;

  @Output() public hideCardCreationFormComponent: EventEmitter<any> = new EventEmitter();

  constructor(private store: Store<StoreState>) { }

  public readonly cardCreationForm: FormGroup = new FormGroup({
    text: new FormControl('')
  });

  public addCard(): void {
    this.store.dispatch(new AddCard(
      this.boardId,
      this.columnId,
      {
        id: uuid(),
        ...this.cardCreationForm.value,
        votesNumber: [],
        comments: []
      }
    ));
    this.hideCardCreationFormComponent.emit();
  }

}
