import { ChangeDetectionStrategy, Component, Output, EventEmitter, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { v4 as uuid } from 'uuid';
import { FormControl, FormGroup } from '@angular/forms';
import { StoreState } from '../store/store.model';
import { AddBoard } from '../store/boards/boards.actions';

@Component({
  selector: 'app-board-creation-form',
  templateUrl: './board-creation-form.component.html',
  styleUrls: [ './board-creation-form.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoardCreationFormComponent {

  @Input() public isBoardCreationFormComponentCancelButtonShown!: boolean;

  @Output() public hideBoardCreationFormComponent: EventEmitter<any> = new EventEmitter();

  constructor(private store: Store<StoreState>) { }

  public readonly boardCreationForm: FormGroup = new FormGroup({
    name: new FormControl('')
  });

  public addBoard(): void {
    this.store.dispatch(new AddBoard({
      id: uuid(),
      ...this.boardCreationForm.value,
      columns: [
        {
          id: uuid(),
          name: 'Went well',
          cards: []
        },
        {
          id: uuid(),
          name: 'To improve',
          cards: []
        },
        {
          id: uuid(),
          name: 'Action items',
          cards: []
        }
      ]
    }));
  }

}
