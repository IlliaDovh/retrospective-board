import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Board } from './shared/board.model';
import { StoreState } from './store/store.model';
import { selectBoards } from './store/boards/boards.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  constructor(private store: Store<StoreState>) { }

  public readonly boards$: Observable<Board[]> = this.store.select(selectBoards);

  isBoardCreationFormComponentShown: boolean;

  isBoardCreationFormComponentCancelButtonShown: boolean;

  ngOnInit(): void {
    this.boards$.subscribe(boards => {
      if (boards.length) {
        this.isBoardCreationFormComponentShown = false;
        this.isBoardCreationFormComponentCancelButtonShown = true;
      } else {
        this.isBoardCreationFormComponentShown = true;
      }
    });
  }

  public hideBoardCreationFormComponent(): void {
    this.isBoardCreationFormComponentShown = false;
  }

  public getBoardCreationFormComponentData({
    isBoardCreationFormComponentShown,
    isBoardCreationFormComponentCancelButtonShown
  }: {
    isBoardCreationFormComponentShown: boolean,
    isBoardCreationFormComponentCancelButtonShown?: boolean
  }): void {
    this.isBoardCreationFormComponentShown = isBoardCreationFormComponentShown;
    if (typeof isBoardCreationFormComponentCancelButtonShown === 'boolean') {
      this.isBoardCreationFormComponentCancelButtonShown = isBoardCreationFormComponentCancelButtonShown;
    }
  }

}
