require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const fs = require('fs').promises;

const PORT = process.env.PORT || 8080;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.get('/boards', async (req, res) => {
    const boards = await fs.readFile('./boards.json', 'utf-8');

    res.send(boards);
});
app.post('/boards', async (req, res) => {
    const updatedState = JSON.stringify(req.body);

    await fs.writeFile('./boards.json', updatedState, 'utf-8');
    res.sendStatus(200);
});
app.use((err, req, res, next) => res.status(500).json({message: err.message}));

app.listen(PORT, () => console.log(`Server started at port ${PORT}!`));
